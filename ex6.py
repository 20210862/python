#int variable declared
types_of_people = 10

#formatting variable
x = f"There are {types_of_people} types of people"

#more variables
binary = "binary"
do_not = "don't"
y=f"Those who know {binary} and those who {do_not}."

print(x)
print(y)

print(f"I said: {x}")
print(f"I also said: '{y}'")

hilarious = False

#notice the curly braces
joke_evaluation = "Isn't that joke so funny?! {}"

#applying a format to an already created string
print(joke_evaluation.format(hilarious))

w = "This is the left side of..."
e = "a string with a right side."

#printing two string concatenated together
print(w + e)