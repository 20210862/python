# unsupervised learning
# where the programs learn the inherent structure of the data based on unlabeled examples
# clustering - commonly used approach that finds patterns and structures in unlabeled data by grouping them into clusters
# i.e. search engines to group similar objects in one cluster
# we don't tell the program anything about what we expect the output to be
# the program analyses the data itself and attempts to find patterns and group the data in meaningful ways
