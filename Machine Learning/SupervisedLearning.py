# supervised learning
# the data is labeled and the program learns to predict the output from the input 
# regression problems - we try to predict a continuous valued output
# i.e. what are the average house prices in London?
# classification - we are tying to predict a discreate number of values
# i.e. is this email spam?
# a program will need examples to recognise an input
# image classification - show a program thousands of examples of pictures with labels to describe them
# the program adjusts its internal parameters
# when the program is shown a new unknown photo, it should be able to product a description of the photo

#linear regression example

# matplotlib is a Python plotting package - which produces 2D graphics
import matplotlib.pyplot as plt 

#new lists
months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
revenue = [34, 43, 84, 110, 130, 105, 135, 130, 147, 144, 175, 199]

# to determine a line, we need a slope and its intercept
# we could say for each point on a line...
# y = mx + b
# m is the slope
# b is the intercept
# y is a given point on the y-axis
# which corresponds to a given x on the x-axis
# with Linear Regression - we need to get the best slope and intercept for the data

# slope - increase this value to make the line steeper
m = 10

# intercept - increase this value to move the line up
b = 55

# new list - iterates through every element in months list and performs the following calculation
# list comprehension
y = [m * x + b for x in months]

plt.plot(months, revenue, "o")

# plot the line
plt.plot(months, y)

#title for the chart
plt.title("Papa Josh's Revenue for 2019")

# give the axis a label
plt.xlabel("Months")
plt.ylabel("Revenue (£)")

plt.show()