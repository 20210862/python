#this script will open a file and print it out
from sys import argv

#we get the file from the user
script, filename = argv

#open a file and return a stream
txt = open(filename)

print(f"Here's your file {filename}:")
#print the txt file
print(txt.read())

#ask for input for filename again
print("Type the filename again:")
file_again = input("> ")

#print file
txt_again = open(file_again)
print(txt_again.read())

#additional commands
#close - closes the file, like saving in an editor
#read - reads the contents of the file, you can assign it to a variable
#readline - reads just one line of a text file
#truncate - empties the file
#write('thing') - writes 'thing' to file
#seek(0) - moves the read/write location to the beginning of the file