#new variable defined
formatter = "{} {} {} {}"

print(formatter.format(1, 2, 3, 4))
print(formatter.format("one", "two", "three", "four"))
print(formatter.format(True, False, False, True))
print(formatter.format(formatter, formatter, formatter, formatter))
print(formatter.format(
    "Try your",
    "Own text here",
    "Maybe a poem",
    "Or a song about fear"
))

#we're calling the format function and passing it four arguments, which match up with the four {} in the formatter variable
#the result of calling format on formatter is a new string that has the {} replaced with the four variables
#that's what print is now printing out!