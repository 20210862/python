#new variables
days = "Mon Tue Wed Thu Fri Sat Sun"
#adding a new line - using the backslash character encodes difficult-to-type characters into a string
#we are using an escape sequences
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

print("Here are the days: ", days)
print("Here are the months: ", months)

#we can use triple quotes to add as many new lines as we want
print("""
There's something going on here.
With the three double-quotes.
We'll be able to type as much as we like.
Even 4 lines if we want, or 5, or 6.
""")