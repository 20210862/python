#we import the argv module (list) from sys
#import = this is how we add features to the script from the Python feature set
#instead of giving all the features at once, Python asks you to what you plan to use
#keeps programs small, also acts as documentation
#argv = argument variable, holds the arguments you pass to the Python script
#we unpack argv so that rather than holding all the arguments, it gets assigned to variables you can work with (script, first, second and third)
#basically, take whatever is in argv, unpack it, and assign it to all these variables on the left in order 
from sys import argv
#when we run this program from a terminal, we need to provide a value for each argument
script, first, second, third = argv

print("The script is called:", script)
print("Your first variable is:", first)
print("Your second variable is:", second)
print("Your third variable is:", third)

age = input("What is your age? ")
print(f"Ah - your age is {age}")