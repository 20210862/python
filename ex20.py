#import argv module from sys
from sys import argv

#two arguments defined
script, input_file = argv

def print_all(f):
    #print the file to the screen
    print(f.read())

def rewind(f):
    #go back the beginning of the file
    f.seek(0)

def print_a_line(line_count, f):
    print(line_count, f.readline())

#open file
current_file = open(input_file)

print("First let's print the whole file:\n")

print_all(current_file)

print("Now let's rewind, kind of like a tape")

rewind(current_file)

print("Let's print three lines:")

current_line = 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)

print("Alright, let's close the file!")

current_file.close()

print("Closed!")