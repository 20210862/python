# import requests library - done by running - pip install requests
import requests
import json
import datetime

# function for viewing the rates
def viewRates(currencyRates):
    print(f"Now viewing rates as of", datetime.datetime.now())
    for key in currencyRates:
        print(key, currencyRates[key])
    enter = input("Press enter to return to the main menu.")

# function for printing currency rates to a file
def printToFile(currencyRates):
    time = datetime.datetime.now()
    # formatting datetime to string format (so that it can be written to a file)
    time.strftime("%d,%m,%Y,%H,%M")
    time = str(time)
    location = input(
        "Please state where you want the currency rates to be written to. \n>")
    print(f"You have specified", location)
    print(f"Now writing CurrencyRates.txt to - {location}")
    # creating a new file, using parameter w - will create a file if the specified file doesn't exist
    # you could use x but the program will fail if the file exists in specified location
    file = open("CurrencyRates.txt", "w")
    # writing to a file only allows text format, so json.dumps takes the dictionary and serialises it to a formatted string
    # write function only allows 1 argument
    file.write(time)
    file.write("\n")
    file.write(json.dumps(currencyRates))
    file.close()
    print("Complete!")
    enter = input("Press enter to return to the main menu.")

# function for swapping currency
def swapCurrency(currencyRates, selection):
    choice = input(f"Please enter the three character code of the currency you want to swap to.\nYou are currently looking at exchange rates for {selection}\n>").upper()
    print(f"You have selected {choice}")
    # try/except block to catch errors
    try:
        # accessing specific element in dictionary
        print(f"{choice} -", currencyRates[choice])
        print(f"The type of the currencyRates[choice] variable is", type(currencyRates[choice]))
        value = input(f"Please enter how much {selection} you would like to convert to {choice}\n>")
        value = float(value)
        print(f"The type of the value variable is,", type(value))
        calculation = value * currencyRates[choice]
        print(f"For {value} {selection} - you will receieve {calculation} {choice}")
        enter = input("Press enter to return to the main menu.")
    except:
        retry = input("An error occurred! Press enter to return to the main menu!")

# boolean for ensuring that a valid currency choice has been entered
check = False

while check == False:
    # take user selection and convert the string to upper
    selection = input(
        "Welcome to Josh Bank! Please enter the three character code for a currency (GBP, USD, JPY).\n> ").upper()
    #selection = selection.upper
    print(f"You have selected {selection}")
    address = "https://api.exchangeratesapi.io/latest?base=" + selection
    print(address)
    rates = requests.get(address)
    # printing the status code returned
    print(f"Status code from request is", rates.status_code)
    # print the content type of the response
    print(f"Content type is", rates.headers['content-type'])
    if rates.status_code == 400:
        print(f"Error! Please try again!")
        print(rates.content)
    if rates.status_code == 200:
        # break while loop
        check = True
# check the type of the rates variable
print(f"The type of variable rates is", type(rates))
# assign the response into a dictionary variable
rawData = rates.json()
print(f"The type of variable rawData is", type(rawData))
# since the data has nested objectes, a new dictionary is created consisting of just the currency rates
# a dictionary in Python is a collection which is unordered, changeable and indexed. No duplicate members are allowed.
currencyRates = rawData['rates']
print(f"The type of variable currencyRates is", type(currencyRates))
# boolean for menu loop
menuLoop = True
while menuLoop == True:
    print("""
Main menu
Enter a character for a menu choice!
(V)iew rates
(P)rint to file
(S)wap currency
(Q)uit application""")
    # since switch statement don't exist in Python, a nested if statement is used
    menuChoice = input(">").upper()
    if menuChoice == 'V':
        # print(currencyRates)
        viewRates(currencyRates)
    elif menuChoice == 'P':
        # print currency rates to an external file
        printToFile(currencyRates)
    if menuChoice == 'S':
        # swap selected currency to another currency
        swapCurrency(currencyRates, selection)
    if menuChoice == 'Q':
        # exist the program
        menuLoop = False
    else:
        # error handling
        print("")
